# Boilerplate countdown component

## Getting started
To get started follow the following steps. When you are done, push the repository to your new branch and [create a PR in GitLab](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html).

1. clone the repo:
    ```bash
    git clone git@gitlab.com:dcatVodafoneZiggo/vfz-boilerplate.git
    ```
2. checkout to a new branch
3. [Install vue and your dependencies](https://docs.npmjs.com/specifying-dependencies-and-devdependencies-in-a-package-json-file)
4. Make your changes (see assignment).
5. [Commit](https://www.conventionalcommits.org/en/v1.0.0/) as much info as you can.
6. Push to GitLab
7. Done!

## Assignment
This component is broken and needs some serious TLC. What we need is a component that counts down to a date in the future. We will use this component to announce the launch of the new iPhone xx!
You can determine any date in the future. Check the boilerplate HTML to see what we want it to display.

> <b>NOTE: Don't just blindly copy paste from the internet, but make the component your own. We want to determine what your level is, and how to guide you when you join a squad and that is not possible when you copy paste everything.</b>

### HTML
The basic HTML is present and it already contains some Vue checks to show/hide specific element. It can use some refactoring and it's up to you what you want to adjust and improve.
We only have some conventions it has to abide by:
- When an element has more than 2 attributes, each attribute should go on a new line, preceded by a tab.
- The order of attributes on a HTML element is: HTML attributes, Vue bindings, Vue events.
- Use Vue shorthands when possible e.g: `v-bind:id` should be `:id`, `v-on:click` should be `@click`

### CSS
Completely missing! We use SCSS and have these following rules:
- Avoid nesting too deep (maximum depth of 3)
- Follow the BEM methodology
- Never style an element, use classes instead

### JS
We build our components with ES6 and VueJS version 2. The boilerplate is empty, but directs you where certain methods and data should go.

### Testing
Create at least 1 unit test. We use the Jasmine testing framework, but feel free to use something else entirely.
